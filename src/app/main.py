from flask import Flask, jsonify, request, redirect, after_this_request, render_template, url_for, render_template
import json
import os
import subprocess

from models.certificates import Certificates

app = Flask(__name__)

@app.route('/get_legacy_certificate', methods=["GET"])
def get_legacy_certificate_action():
    for_service = request.args.get("service_name")
    print("CERT REQUESTED FOR", for_service)
    return_value = Certificates().get_legacy_certificates_for(service_name=for_service)
    return jsonify(return_value), 200


@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@app.before_request
def before_request():
    payload = request.get_json(silent=True)
    if payload is not None:
        payload["correlation_id"] = request.headers.get("correlation_id", "")
        request.data = json.dumps(payload)
