import os
from pathlib import Path

class CertificateNotFound(Exception):
     pass

class Certificates():
    def __init__(self):
        pass

    def get_legacy_certificates_for(self, service_name):
        return_value = {
            "id_rsa": self.get_legacy_idrsa(service_name=service_name),
            "id_rsa.pem": self.get_legacy_idrsa_pem(service_name=service_name),
            "id_rsa.pub": self.get_legacy_idrsa_pub(service_name=service_name)
        }

        return return_value

    def get_legacy_idrsa(self, service_name):
        return self.load_file_content(service_name=service_name, file_name="id_rsa")

    def get_legacy_idrsa_pem(self, service_name):
        return self.load_file_content(service_name=service_name, file_name="id_rsa.pem")

    def get_legacy_idrsa_pub(self, service_name):
        return self.load_file_content(service_name=service_name, file_name="id_rsa.pub")

    def load_file_content(self, service_name, file_name):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file_name = f"{dir_path}/../../certs/{service_name.replace('.', '')}/{file_name}"
        if not Path(file_name).exists():
            raise CertificateNotFound(f"No certificate of type {file_name} found for service {service_name}")
        else:
            with open(file_name) as f:
                file_content = f.read()
            return file_content

    # def create_keys():
    #     if os.path.isfile("/src/certs/id_rsa") or os.path.isfile("/src/certs/id_rsa.pem"):
    #         return
    #     (pubkey, privkey) = rsa.newkeys(2048, True, 8)
    #
    #     with open("/src/certs/id_rsa.pem", "w") as text_file:
    #         text_file.write(pubkey.save_pkcs1().decode('ascii'))
    #     with open("/src/certs/id_rsa", "w") as text_file:
    #         text_file.write(privkey.save_pkcs1().decode('ascii'))