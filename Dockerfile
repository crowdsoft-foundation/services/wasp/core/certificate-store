FROM registry.gitlab.com/crowdsoft-foundation/various/python-base-image:production

ENV CLUSTER_CONFIG '{"environment": "development"}'

COPY src/requirements.txt /src/app/requirements.txt
RUN pip3 install -r /src/app/requirements.txt

COPY src /src

EXPOSE 8000

RUN chmod 777 /src/runApp.sh
RUN chmod +x /src/runApp.sh
RUN chmod 777 /src/testApp.sh
RUN chmod +x /src/testApp.sh
WORKDIR "/src/app"
ENTRYPOINT ["/src/runApp.sh", "-r"]
